Talk
===

What is bash?

- a programmable shell
- consider also: fish, zsh, ksh, command.com, cmd.exe, pash, csh, sh, dash, etc...

Why choose bash?

- ubiquity
- better than most
- about as good as the rest


Demo
===

- Alluvial programming

What is the most popular shell on this system?

```bash
cat /etc/passwd
cat /etc/passwd | cut -f 7 -d :
cat /etc/passwd | cut -f 7 -d : | sort
cat /etc/passwd | cut -f 7 -d : | sort | uniq -c
cat /etc/passwd | cut -f 7 -d : | sort | uniq -c | sort -rn
cat /etc/passwd | cut -f 7 -d : | sort | uniq -c | sort -rn | tail -n 1
cat /etc/passwd | cut -f 7 -d : | sort | uniq -c | sort -rn | head -n 1
cat /etc/passwd | cut -f 7 -d : | sort | uniq -c | sort -rn | head -n 1 | awk '{ print $2 }'
```

Hello Bob! Would you like to play a game?

```bash
echo "Hello World!"
greeting=hello name=bob echo "$greeting $name\!"
greeting=hello name=bob ; echo $greeting $name\!
greeting=hello name=bob && echo $greeting $name\!
greeting=hello name=david && echo $greeting $name\!
greeting=hello name=$( read -p "name? " ) && echo $greeting $name\
greeting=hello ; read -p "name? " name && echo $greeting $name\!
help read
read -n 1 choice; echo choice
read -p "Would you like to play a game? " -n 1 choice; echo choice=$choice
if [[ $choice =~ [yY] ]]; then echo choice was like yes; else echo choice was not like yes; fi
```

- Stupid history tricks
  + [Control]+[R], pattern
  + `history | less`, [Control]+[LeftMouse]+Drag
  + grep `~/.bash_history`, HISTFILESIZE, HISTLENGTH

- Finally, some scripting
  + see hello-bob
  + see most-popular-shell
