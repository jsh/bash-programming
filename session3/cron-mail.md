# Cron and mail

By default, output and errors from a job go to the job-owner as email.

One common practice is to log them, instead. Cron suppresses empty mail messages.

A user's mail can be redirected from their `~/.forward` file or a line in /etc/aliases. Whenever `/etc/aliases` is changed, `newaliases` must be run.

User's crontabs are in /var/spool/cron/crontabs/$USER (or something like that)

User's mail Inbox is /var/spool/mail/$USER
