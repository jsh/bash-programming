#!/bin/bash -eu

[[ $( lsb_release -r | tr -d . | awk '{ print $2 }' ) -ge 1710 ]] &&
  alias apt-get=apt

sudo apt-get update
sudo apt-get -y autoremove

boot_min_free=256 # in MB
root_min_free=2048

if [[ $(( $( df / | tail -n 1 | awk '{ print $4 }' ) / 1024 )) -lt $root_min_free ]]
then
  echo "Free space on the root filesystem is low."
  echo "Try: sudo baobab, ncdu -x /, du -sm /*, or so."
  echo "Quitting..."
  exit 1
fi

while [[ $(( $( df /boot | tail -n 1 | awk '{ print $4 }' ) / 1024 )) -lt $boot_min_free ]]
do
  echo "Free space on /boot is low."
  package_to_remove=''
  for f in $( ls -S /boot | grep -v $( uname -r ) | grep -v memtest )
  do
    if dpkg -S /boot/$f &> /dev/null
    then
      largest_associated_file=$f
      echo "The largest file on /boot unrelated to the currently running kernel but related to an installed package is: $largest_associated_file"
      package_to_remove=$( dpkg -S /boot/$f | cut -f 1 -d : )
      echo "The package with which it is associated is: $package_to_remove"
      break
    fi
  done
  if [[ -z $package_to_remove ]]
  then
    echo "/boot is still too full, but I've found nothing to remove"
    exit 1
  else
    sudo apt-get purge $package_to_remove
    sudo apt-get -y autoremove
  fi
  sleep 3
done

sudo apt-get -y dist-upgrade
sudo apt-get -y autoremove
sudo shutdown --reboot +10 "This system is rebooting to complete maintenance."
