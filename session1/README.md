talk

Sysadmins sometimes have to patch systems and clean up log files.

Programmers sometimes have to write programs to create crap to use to test their other programs.

Wouldn't it be nice to have a patch script you could use on any system?

I strongly recommend a monthly patch and reboot for long-lived machines. I also recommend against long-lived machines. The pets versus cattle saw, but we all *have* long-lived machines in the real world, don't we? So, patch and reboot, the oftener the better.

loops ( | while read ), globs, brace expansions, touch, find, command substitution

demo:
- fully update a Linux system *without* knowing which package manager it uses (apt/yum/zypper/dnf).
- delete old logs
- create old logs (needed for #2)
